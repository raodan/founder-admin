var gulp = require('gulp'),
  jshint = require('gulp-jshint'),
  usemin = require('gulp-usemin'),
  connect = require('gulp-connect'),
  watch = require('gulp-watch'),
  webpack = require('webpack-stream'),
  minifyCss = require('gulp-minify-css'),
  minifyJs = require('gulp-uglify'),
  concat = require('gulp-concat'),
  rename = require('gulp-rename');

var paths = {
  srcFiles: ['src/**/*.*'],
  entry: 'src/index.js',
  index: 'index.html',
  staticRes: 'static/**/*.*',
  externalRes: 'external/**/*.*',
  fonts: [
    'node_modules/bootstrap/dist/fonts/*.{woff, woff2}', 
    'node_modules/font-awesome/fonts/*.{woff, woff2}', 
    'bower_components/rdash-ui/dist/fonts/*.{woff, woff2}'],
  distRes: [
    'bower_components/octicons/octicons/*.{woff, woff2}'],
  dstFiles: ['build/**/*.*']
};

/**
 * Handle bower components from index
 */
gulp.task('usemin', function() {
  return gulp.src(paths.index)
    .pipe(usemin({
      js: [minifyJs(), 'concat'],
      css: [minifyCss({
        keepSpecialComments: 0
      }), 'concat'],
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('webpack', function() {
  return gulp.src(paths.entry)
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(rename('build.js'))
    .pipe(gulp.dest('build/dist/'));
});

/**
 * Copy assets
 */
gulp.task('build-assets', ['copy-fonts', 'copy-dist']);

gulp.task('copy-fonts', function() {
  return gulp.src(paths.fonts)
    .pipe(rename({
      dirname: '/fonts'
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('copy-dist', function() {
  return gulp.src(paths.distRes)
    .pipe(rename({
      dirname: '/dist'
    }))
    .pipe(gulp.dest('build/'));
});

/**
 * Handle custom files
 */
gulp.task('build-custom', ['copy-static', 'copy-external']);

gulp.task('copy-static', function() {
  return gulp.src(paths.staticRes)
    .pipe(gulp.dest('build/static'));
});

gulp.task('copy-external', function() {
  return gulp.src(paths.externalRes)
    .pipe(gulp.dest('build/external'));
});

/**
 * Live reload server
 */
gulp.task('webserver', function() {
  connect.server({
    root: 'build/',
    livereload: true,
    port: 5000
  });
});

gulp.task('livereload', function() {
  gulp.src(paths.dstFiles)
    .pipe(connect.reload());
});

/**
 * Watch custom files
 */
gulp.task('watch', function() {
  gulp.watch([paths.staticRes], ['copy-static', 'usemin']);
  gulp.watch([paths.externalRes], ['copy-external']);
  gulp.watch([paths.index], ['usemin']);
  gulp.watch(paths.srcFiles, ['webpack']);
  gulp.watch(paths.dstFiles, ['livereload']);
});

/**
 * Gulp tasks
 */
gulp.task('build', ['webpack', 'usemin', 'build-assets', 'build-custom']);
gulp.task('default', ['build', 'webserver', 'watch']);