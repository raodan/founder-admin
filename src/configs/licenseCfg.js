module.exports = [{
  text: '授权人',
  field: 'licensor',
  value: ''
}, {
  text: '授权时间',
  field: 'authTime',
  value: ''
}, {
  text: '项目编号',
  field: 'projectNo',
  value: ''
}, {
  text: '项目描述',
  field: 'projectDesc',
  value: ''
}, {
  text: '最大UI连接数',
  field: 'maxUIConnections',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '最大拼接屏数',
  field: 'maxDecOfLcdWall',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '最大网络摄像机列表（管理）数目',
  field: 'maxListCntOfIpc',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '同时录播通道数',
  field: 'maxRecordChannel',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '同时点播上屏通道数',
  field: 'maxPlayChannel',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '最大软编路数',
  field: 'maxListCntOfSoftEnc',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '最大代理通道数',
  field: 'maxListCntOfProxy',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '最大分享通道数',
  field: 'maxListCntOfShare',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '最大KVM连接数',
  field: 'maxConnCntOfKVM',
  value: 0,
  show: function(value) {
    return value < 0 ? '无效' : value;
  }
}, {
  text: '中心服务备份',
  field: 'enableCluster',
  value: 0,
  show: function(value) {
    return value === 0 ? '未启用' : '启用';
  }
}, {
  text: '试用开始时间',
  field: 'trialBeginTime',
  value: '',
  show: function(value) {
    return value === '' ? '无限制' : value;
  }
}, {
  text: '试用结束时间',
  field: 'trialEndTime',
  value: '',
  show: function(value) {
    return value === '' ? '无限制' : value;
  }
}]