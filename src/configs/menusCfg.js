var buildBranch = require('../lib/settings').buildBranch;

module.exports = [{
  name: '系统设置',
  icon: 'menu-icon fa fa-cogs fa-lg',
  url: '/system',
  filterBy: '*',
  product: 'Codec|Media'
}, {
  name: '系统工具',
  icon: 'menu-icon fa fa-dashboard fa-lg',
  url: '/systool',
  filterBy: '*',
  product: 'Codec'
}, {
  name: '系统状态',
  icon: 'menu-icon fa fa-area-chart fa-lg',
  url: '/sysstate',
  filterBy: '*',
  product: 'Codec|Media'
}, {
  name: '编解码器',
  icon: 'menu-icon fa fa-film fa-lg',
  url: '/devcodec',
  filterBy: 'Codec',
  product: 'Codec'
}, {
  name: '录播文件',
  icon: 'menu-icon fa fa-file fa-lg',
  url: '/records',
  filterBy: 'MediaServer',
  product: 'Media'
}, {
  name: '系统设置', //主服务器
  icon: 'menu-icon fa fa-server fa-lg',
  url: '/mainserver',
  filterBy: 'Server',
  product: 'Server'
}, {
  name: '流媒体源',
  icon: 'menu-icon fa fa-file-movie-o fa-lg',
  url: '/ms_stream',
  filterBy: 'Server',
  product: 'Server'
}, {
  name: '媒体管理',
  icon: 'menu-icon fa fa-desktop fa-lg',
  url: '/ms_multimedia',
  filterBy: 'Server',
  product: 'Server'
}, {
  name: '设备管理',
  icon: 'menu-icon fa fa-cubes fa-lg',
  url: '/ms_devmng',
  filterBy: 'Server',
  product: 'Server'
}, {
  name: '背景设置',
  icon: 'menu-icon fa fa-photo fa-lg',
  url: '/ms_background',
  filterBy: 'Server',
  product: 'Server'
}, {
  name: 'Web应用',
  icon: 'menu-icon fa fa-arrows fa-lg',
  url: '/webapp',
  filterBy: buildBranch ? 'Server' : 'None',
  product: 'Server'
}, {
  name: '脚本扩展',
  icon: 'menu-icon fa fa-arrows fa-lg',
  url: '/scriptplugin',
  filterBy: buildBranch ? 'Server' : 'None',
  product: 'Server'
}, {
  name: '中控管理',
  icon: 'menu-icon fa fa-gamepad fa-lg',
  url: '/centerctrl',
  filterBy: 'None',
  product: 'Server'
}, {
  name: '开发',
  icon: 'menu-icon fa fa-terminal fa-lg',
  url: '/develop',
  filterBy: 'Debug',
  product: '*'
}, {
  name: 'Websocket',
  icon: 'menu-icon fa fa-terminal fa-lg',
  url: '/wstest',
  filterBy: 'Debug',
  product: '*'
}, {
  name: 'VLC测试',
  icon: 'menu-icon fa fa-bug fa-lg',
  url: '/vlctest',
  filterBy: 'Debug',
  product: 'Media'
}, {
  name: '流媒体测试',
  icon: 'menu-icon fa fa-bug fa-lg',
  url: '/streamtest',
  filterBy: 'Debug',
  product: 'Codec'
}, {
  name: '服务测试',
  icon: 'menu-icon fa fa-bug fa-lg',
  url: '/servertest',
  filterBy: 'Debug',
  product: 'Server'
}, {
  name: '自定义UI',
  icon: 'menu-icon fa fa-bug fa-lg',
  url: '/customui',
  filterBy: 'Debug',
  product: 'Server'
}]