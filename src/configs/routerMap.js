var settings = require('../lib/settings');
var productCfg = settings.productCfg;
var authState = settings.authState;

var codecRouterMap = {
  '/': {
    component: require('../views/system.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/signin': {
    component: require('../views/dev-signin.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/systool': {
    component: require('../views/systool.vue'),
    belongTo: 'Codec',
    authState: authState
  },
  '/sysstate': {
    component: require('../views/sysstate.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/devcodec': {
    component: require('../views/dev-codec.vue'),
    belongTo: 'Codec',
    authState: authState
  },
  '/system': {
    component: require('../views/system.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/develop': {
    component: require('../views/develop.vue'),
    belongTo: '*',
    authState: authState
  },
  '/wstest': {
    component: require('../views/wstest.vue'),
    belongTo: '*',
    authState: authState
  },
  '/streamtest': {
    component: require('../views/streamtest.vue'),
    belongTo: 'Codec',
    authState: authState
  }
};

var mediaRouterMap = {
  '/': {
    component: require('../views/system.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/signin': {
    component: require('../views/dev-signin.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/sysstate': {
    component: require('../views/sysstate.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/system': {
    component: require('../views/system.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/records': {
    component: require('../views/media-records.vue'),
    belongTo: 'MediaServer',
    authState: authState
  },
  '/develop': {
    component: require('../views/develop.vue'),
    belongTo: '*',
    authState: authState
  },
  '/wstest': {
    component: require('../views/wstest.vue'),
    belongTo: '*',
    authState: authState
  },
  '/vlctest': {
    component: require('../views/vlctest.vue'),
    belongTo: 'MediaServer',
    authState: authState
  }
};

var serverRouterMap = {
  '/': {
    component: require('../views/main-server.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/signin': {
    component: require('../views/dev-signin.vue'),
    belongTo: productCfg.dftModule,
    authState: authState
  },
  '/ms_stream': {
    component: require('../views/main-server-stream.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/ms_multimedia': {
    component: require('../views/main-server-multimedia.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/ms_devmng': {
    component: require('../views/main-server-devmng.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/ms_background': {
    component: require('../views/main-server-background.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/webapp': {
    component: require('../views/my-webapp.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/scriptplugin': {
    component: require('../views/my-script-plugin.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/mainserver': {
    component: require('../views/main-server.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/centerctrl': {
    component: require('../views/centerctrl.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/develop': {
    component: require('../views/develop.vue'),
    belongTo: '*',
    authState: authState
  },
  '/wstest': {
    component: require('../views/wstest.vue'),
    belongTo: '*',
    authState: authState
  },
  '/servertest': {
    component: require('../views/servertest.vue'),
    belongTo: 'Server',
    authState: authState
  },
  '/customui': {
    component: require('../views/customui.vue'),
    belongTo: 'Server',
    authState: authState
  }
};

function getRouterMap() {
  if (productCfg.product === 'Media') {
    return mediaRouterMap;
  } else if (productCfg.product === 'Codec') {
    return codecRouterMap;
  } else if (productCfg.product === 'Server') {
    return serverRouterMap;
  } else {
    return {};
  }
}

module.exports = getRouterMap();