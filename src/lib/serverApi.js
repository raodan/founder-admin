var WebApi = require('./webApi');

exports.system = new WebApi('Server', 'sys', 0);
exports.streamsrc = new WebApi('Server', 'streamsrc', 0);
exports.multimedia = new WebApi('Server', 'multimedia', 0);
exports.devmng = new WebApi('Server', 'devmng', 0);
exports.mediaPhy = new WebApi('Server', 'mediaPhy', -1);
exports.media = new WebApi('Server', 'media', 0);
exports.transfer = new WebApi('Server', 'transfer', -1);
exports.cluster = new WebApi('Server', 'cluster', 0, function makePrefix() {
  return '/api/cluster';
});