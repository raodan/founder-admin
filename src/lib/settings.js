var utils = require('./utils');

var HOST_NAME_FOR_DEBUG = '192.168.1.179'
//Server Codec Media
var HOST_PRODUCT_FOR_DEBUG = 'Codec'

var buildBranch = false;
exports.buildBranch = buildBranch;

var authState = {
  signin: false
};

exports.authState = authState;

var debugCfg = {
  TEST_TRACE_ON: false,
  DEBUG_TRACE_ON: true,
  INFO_TRACE_ON: true,
  ERROR_TRACE_ON: true
};

var releaseCfg = {
  TEST_TRACE_ON: false,
  DEBUG_TRACE_ON: false,
  INFO_TRACE_ON: false,
  ERROR_TRACE_ON: true
};

function getHostName() {
  if (isInDebugMode) {
    var args = utils.getQueryStringArgs();
    if (args && args['hostname']) {
      console.log('hostname: ' + args['hostname']);
      return args['hostname'];
    } else {
      return HOST_NAME_FOR_DEBUG;
    }
  } else {
    return window.location.hostname;
  }
}

function getRealProduct() {
  var port = window.location.port;
  if (port === '8080') {
    return 'Codec';
  } else if (port == '6080') {
    return 'Media';
  } else if (port == '9080') {
    return 'Server';
  } else {
    return null;
  }
}

function getProduct() {
  var product = getRealProduct();
  return null != product ? product : HOST_PRODUCT_FOR_DEBUG;
}

function checkInDebugMode() {
  var product = getRealProduct();
  return null != product ? false : true;
}

var isInDebugMode = checkInDebugMode();
var hostname = getHostName();

exports.isInDebugMode = isInDebugMode;

exports.globalCfg = isInDebugMode ? debugCfg : releaseCfg;

exports.hostname = hostname;

function getProductCfg() {
  var cfgs = [{
    product: 'Codec',
    dftModule: 'Platform',
    bootTime: 1*60*1000,
    showDisk: function(item) {
      if (item.mounted === '/opt/config') {
        return '用户参数区';
      } else if (item.mounted === '/opt/user_data') {
        return '用户数据区';
      } else {
        return null;
      }
    },
    hostCfgs: [{
      moduleName: 'Platform',
      port: '8080',
      host: hostname + ':8080'
    }, {
      moduleName: 'Codec',
      port: '7080',
      host: hostname + ':7080'
    }]
  }, {
    product: 'Media',
    dftModule: 'MediaServer',
    bootTime: 6*60*1000,
    showDisk: function(item) {
      var findIndex = item.mounted.indexOf('/data');
      if (findIndex == 0) {
        var index = parseInt(item.mounted.substr(5));
        if (!index) {
          index = 0;
        }
        index++;
        return '媒体文件区' + index;
      } else {
        return null;
      }
    },
    hostCfgs: [{
      moduleName: 'MediaServer',
      port: '6080',
      host: hostname + ':6080'
    }]
  }, {
    product: 'Server',
    dftModule: 'Server',
    bootTime: 4*60*1000,
    hostCfgs: [{
      moduleName: 'Server',
      port: '9080',
      host: hostname + ':9080'
    }]
  }];
  var product = getProduct();

  for (var i = 0; i < cfgs.length; i++) {
    if (cfgs[i].product === product) {
      return cfgs[i];
    }
  };

  return cfgs[0];
}
var productCfg = getProductCfg();
exports.productCfg = productCfg;