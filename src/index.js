/**
 * Boot up the Vue instance and wire up the router.
 */

var Vue = require('vue');
//Vue.config.debug = true;
var VueRouter = require('vue-router');
var api = require('./lib/api');
var settings = require('./lib/settings');
var globalCfg = settings.globalCfg;
var isInDebugMode = settings.isInDebugMode;
var log = require('./lib/log');
var utils = require('./lib/utils');
var productCfg = settings.productCfg;
var authState = settings.authState;

var App = Vue.extend({
  components: {
    //vue-strap components
    vsAccordion:     VueStrap.accordion,
    vsAffix:         VueStrap.affix,
    vsAlert:         VueStrap.alert,
    vsAside:         VueStrap.aside,
    vsCheckboxBtn:   VueStrap.checkboxBtn,
    vsCheckboxGroup: VueStrap.checkboxGroup,
    vsCarousel:      VueStrap.carousel,
    vsDatepicker:    VueStrap.datepicker,
    vsDropdown:      VueStrap.dropdown,
    vsModal:         VueStrap.modal,
    vsOption:        VueStrap.option,
    vsPanel:         VueStrap.panel,
    vsPopover:       VueStrap.popover,
    vsProgressbar:   VueStrap.progressbar,
    vsRadioBtn:      VueStrap.radioBtn,
    vsRadioGroup:    VueStrap.radioGroup,
    vsSelect:        VueStrap.select,
    vsSlider:        VueStrap.slider,
    vsTab:           VueStrap.tab,
    vsTabset:        VueStrap.tabset,
    vsTooltip:       VueStrap.tooltip,
    vsTypeahead:     VueStrap.typeahead,
    //rdash
    rdLinkTable:       require('./components/rd-link-table.vue'),
    rdWidget:          require('./components/rd-widget.vue'),
    rdWidgetHeader:    require('./components/rd-widget-header.vue'),
    rdWidgetBody:      require('./components/rd-widget-body.vue'),
    rdWidgetFooter:    require('./components/rd-widget-footer.vue'),
    appHeader:         require('./views/header.vue'),
    appSidebar:        require('./views/sidebar.vue'),
    //self
    myTable:           require('./components/my-table.vue')
  },
  data: function() {
    return {
      toggle: true,
      sidebar: {
        header: {
          title: '',
          name: '导航',
        },
        menus: require('./configs/menusCfg'),
        footers: [{
          name: 'Tendzone',
          url: 'http://www.tendzone.com/'
        }, {
          name: 'About',
          url: 'http://www.tendzone.com/'
        }, {
          name: 'Support',
          url: 'http://www.tendzone.com/'
        }]
      },
      header: {
        userImgSrc: 'static/img/avatar.jpg'
      },
      productCfg: {
        product: '',
        encoderEn: false,
        decoderEn: false,
        serverEn: false,
        serverWithPcEnv: false,
        devModel: ''
      },
      modules: []
    }
  },
  ready: function() {
    this.productCfg.product = productCfg.product;
    this.modules.splice(0, 0, productCfg.dftModule);
    api.utils.getPlatformCfg(function(res) {
      if (res && res.code == 0) {
        var platformCfg = res.data;

        this.productCfg.devModel = platformCfg.devModel

        if (platformCfg.devType === 'server.media') {
          this.sidebar.header.title = 'M-MD-SERVER';
        } else if (platformCfg.devType === 'server.main') {
          this.sidebar.header.title = 'M-CT-SERVER';
          if (platformCfg.devModel.indexOf('pc') != -1) {
            this.productCfg.serverWithPcEnv = true;
          }
        } else if (platformCfg.devType === 'encoder') {
          this.modules.splice(0, 0, 'Codec');
          this.productCfg.encoderEn = true;
          if (platformCfg.devModel === 'codec.founder100e') {
            this.sidebar.header.title = 'FOUNDER-100E';
          } else if (platformCfg.devModel === 'codec.founder100es') {
            this.sidebar.header.title = 'FOUNDER-100ES';
          } else if (platformCfg.devModel === 'codec.founder108e') {
            this.sidebar.header.title = 'FOUNDER-108E';
          } else if (platformCfg.devModel === 'codec.founder108es') {
            this.sidebar.header.title = 'FOUNDER-108ES';
          }
        } else if (platformCfg.devType === 'decoder') {
          this.modules.splice(0, 0, 'Codec');
          this.productCfg.decoderEn = true;
          if (platformCfg.devModel === 'codec.founder100d') {
            this.sidebar.header.title = 'FOUNDER-100D';
          } else if (platformCfg.devModel === 'codec.founder100ds') {
            this.sidebar.header.title = 'FOUNDER-100DS';
          } else if (platformCfg.devModel === 'codec.founder108d') {
            this.sidebar.header.title = 'FOUNDER-108D';
          } else if (platformCfg.devModel === 'codec.founder100scr') {
            this.sidebar.header.title = 'FOUNDER-100SCR';
          }
        }

        var runTags = JSON.parse(platformCfg.runTags);
        for (var i = 0; i < runTags.length; i++) {
          if (runTags[i] === 'mainServer') {
            this.productCfg.serverEn = true;
          }
        };
      }
    }.bind(this)); 
  }
});

Vue.use(VueRouter);

var router = new VueRouter({
  hashbang: true,
  history: false
});

var routerMap = require('./configs/routerMap');
router.map(routerMap);

router.beforeEach(function(transition) {
  if (authState.signin != true 
    && transition.to.path != '/signin') {
    transition.abort();
    router.go('/signin');
    return;
  }

  log.DEBUG_TRACE('from:' + transition.from.path + ':' + transition.from.belongTo);
  log.DEBUG_TRACE('to:' + transition.to.path + ':' + transition.to.belongTo);
  
  api.switchModule(transition.to.belongTo, function() {
    transition.next();
  });
});

router.start(App, '#page-wrapper');