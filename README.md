#founder-admin

本工程不再新增功能，全部功能基于vue1.x版本重构，见[新版本](http://git.oschina.net/raodan/midis-admin)

###运行指南
```
git clone https://git.oschina.net/raodan/founder-admin.git
cd founder-admin
npm install
bower install

gulp build
or
gulp
```